<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string( 'first_name');
            $table->string( 'other_name')->nullable();
            $table->string( 'last_name');
            $table->string( 'sex');
            $table->string( 'dob');
            $table->string( 'marital_status');
            $table->string( 'profession');
            $table->string( 'employer');
            $table->text( 'home_address');
            $table->text( 'office_address');
            $table->string( 'email');
            $table->string( 'phone');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
