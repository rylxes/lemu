<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ModelSettings;
use Faker\Generator as Faker;

$factory->define(ModelSettings::class, function (Faker $faker) {

    return [
        'model_id' => $faker->word,
        'model_type' => $faker->word,
        'settings' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
