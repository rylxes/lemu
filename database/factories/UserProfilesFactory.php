<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserProfiles;
use Faker\Generator as Faker;

$factory->define(UserProfiles::class, function (Faker $faker) {

    return [
        'title_id' => $faker->randomDigitNotNull,
        'first_name' => $faker->word,
        'other_name' => $faker->word,
        'last_name' => $faker->word,
        'sex' => $faker->word,
        'dob' => $faker->word,
        'marital_status' => $faker->word,
        'profession' => $faker->word,
        'employer' => $faker->word,
        'home_address' => $faker->word,
        'office_address' => $faker->word,
        'email' => $faker->word,
        'phone' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
