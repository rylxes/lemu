<!-- Title Id Field -->


{{--<div class="col-md-12">--}}
{{--<label>Upload Picture</label>--}}
{{--<div class="input-group">--}}
{{--<input type="file" required class="file-upload" name="passport"/>--}}
{{--</div>--}}
{{--</div>--}}


<div class="form-group  {{ $errors->has('passport') ? 'has-error' : ''}}">
    {!! Form::label('passport', 'Upload Picture', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::file('passport',  null, ['class' => 'form-control', 'placeholder' => 'Upload Picture']) !!}
        {!! $errors->first('passport', '<p class="help-block">:message</p>') !!}
    </div>

</div>

<div class="form-group  {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('title', $titles, null, ['class' => 'form-control', 'placeholder' => 'Pick a Title']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group  {{ $errors->has('first_name') ? 'has-error' : ''}}">
    {!! Form::label('first_name', 'First Name', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('first_name',  null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>

</div>

<div class="form-group  {{ $errors->has('other_name') ? 'has-error' : ''}}">
    {!! Form::label('other_name', 'Other Name', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('other_name',  null, ['class' => 'form-control', 'placeholder' => 'Other Name']) !!}
        {!! $errors->first('other_name', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group  {{ $errors->has('last_name') ? 'has-error' : ''}}">
    {!! Form::label('last_name', 'Last Name', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('last_name',  null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group {{ $errors->has('sex') ? 'has-error' : ''}}">
    {!! Form::label('gender', 'Gender', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        <select class="form-control" id="sex" name="sex">
            <option value="">Pick a gender...</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
        {!! $errors->first('sex', '<p class="help-block">:message</p>') !!}

    </div>
</div>

<div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
    {!! Form::label('dob', 'Date of Birth', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('dob', null, ['class' => 'form-control datepicker','editable'=>'false']) !!}
        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
        {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group  {{ $errors->has('marital_status') ? 'has-error' : ''}}">
    {!! Form::label('marital_status', 'Marital Status', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('marital_status', $marital, null, ['class' => 'form-control', 'placeholder' => 'Marital Status']) !!}
        {!! $errors->first('marital_status', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group  {{ $errors->has('profession') ? 'has-error' : ''}}">
    {!! Form::label('profession', 'Profession', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('profession',  null, ['class' => 'form-control', 'placeholder' => 'Profession']) !!}
        {!! $errors->first('profession', '<p class="help-block">:message</p>') !!}
    </div>

</div>

<div class="form-group  {{ $errors->has('employer') ? 'has-error' : ''}}">
    {!! Form::label('employer', 'Employer', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('employer',  null, ['class' => 'form-control', 'placeholder' => 'Employer']) !!}
        {!! $errors->first('employer', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group  {{ $errors->has('home_address') ? 'has-error' : ''}}">
    {!! Form::label('home_address', 'Home Address', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('home_address',  null, ['class' => 'form-control', 'placeholder' => 'Home Address','rows' => 3, 'cols' => 40]) !!}
        {!! $errors->first('home_address', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group  {{ $errors->has('office_address') ? 'has-error' : ''}}">
    {!! Form::label('office_address', 'Office Address', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('office_address',  null, ['class' => 'form-control', 'placeholder' => 'Office Address','rows' => 3, 'cols' => 40]) !!}
        {!! $errors->first('office_address', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group  {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::email('email',  null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group  {{ $errors->has('phone') ? 'has-error' : ''}}">
    {!! Form::label('phone', 'Phone', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('phone',  null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group  {{ $errors->has('next_of_kin') ? 'has-error' : ''}}">
    {!! Form::label('next_of_kin', 'Next of Kin', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('next_of_kin',  null, ['class' => 'form-control', 'placeholder' => 'Next of Kin']) !!}
        {!! $errors->first('next_of_kin', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<div class="form-group  {{ $errors->has('next_of_kin_phone') ? 'has-error' : ''}}">
    {!! Form::label('next_of_kin_phone', 'Next of Kin Phone', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('next_of_kin_phone',  null, ['class' => 'form-control', 'placeholder' => 'Next of Kin Phone']) !!}
        {!! $errors->first('next_of_kin_phone', '<p class="help-block">:message</p>') !!}
    </div>

</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save and Make Payment', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('userProfiles.index') }}" class="btn btn-secondary">Cancel</a>
</div>
