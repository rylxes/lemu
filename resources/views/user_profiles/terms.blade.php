<link rel="stylesheet" href="{{asset('css/style.css')}}">
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <th colspan="2">ITIKAF RULES</th>
        </thead>
        <tbody>
        <tr>
            <td>
                1.
            </td>
            <td>
                Participants must be tolerant of one another.
            </td>
        </tr>


        <tr>
            <td>
                2.
            </td>
            <td>
                Participants should not raise their voices inside the Masjid. Participants should desist from acts that
                may
                inconvenience, distract or harm other participants.
            </td>
        </tr>


        <tr>
            <td>
                3.
            </td>
            <td>
                Fights or quarrels are not allowed.
            </td>
        </tr>


        <tr>
            <td>
                4.
            </td>
            <td>
                ID Cards must be worn at all times.
            </td>
        </tr>


        <tr>
            <td>
                5.
            </td>
            <td>
                Bed spaces will be allocated to each participant. Participants must maintain the allocated bed spaces
                throughout the Itikaf period as this will reduce any inconvenience during daily routine security checks.
            </td>
        </tr>


        <tr>
            <td>
                6.
            </td>
            <td>
                Every participant must come with either a mattress or bed spread or mat or some other covering.
                Participants will not be allowed to sleep on the bare Masjid rug.
            </td>
        </tr>


        <tr>
            <td>
                7.
            </td>
            <td>
                Informal gatherings in groups (Males Only, Females Only or Mixed Gender Groups) around the precincts of
                the Masjid for parties, leisure/relaxation time and unrelated activities are not allowed, and should be
                completely avoided.
            </td>
        </tr>


        <tr>
            <td>
                8.
            </td>
            <td>
                Phones must be switched off or put on silent mode within the Masjid.
            </td>
        </tr>


        <tr>
            <td>
                9.
            </td>
            <td>
                Cooking within the precincts of the Masjid is not allowed.
            </td>
        </tr>


        <tr>
            <td>
                10.
            </td>
            <td>
                Boiling rings, gas burners/cylinders, electric kettles, and pressing irons are not allowed. The items
                will be seized if found within the precincts of the Masjid.
            </td>
        </tr>


        <tr>
            <td>
                11.
            </td>
            <td>
                Participants are not allowed to cook within the precincts of the Masjid. Food vendors will sell various
                food items during Suhoor and Iftar within the Jubril Ayinla Multipurpose Hall at affordable prices.
                Participants are advised to come with sufficient funds to cover their feeding needs.
            </td>
        </tr>


        <tr>
            <td>
                12.
            </td>
            <td>
                Only participants that have paid the required Itikaf Fee will be allowed to sleep within the precints of
                the Masjid throuighout the Itikaf period. Participants should not come along with any non-paying family
                member or friend
            </td>
        </tr>

        <tr>
            <td>
                13.
            </td>
            <td>
                Management reserves the right to introduce new rules during the Itikaf period and participants are bound
                to obey the rules as prescribed.
            </td>
        </tr>


        <tr>
            <td>
                14.
            </td>
            <td>
                Management reserves the right to decamp any participant for breaching any of the rules itemised above or
                introduced during the Itikaf period.
            </td>
        </tr>


        </tbody>
    </table>
</div>




