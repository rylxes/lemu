<div class="form-group">
    <div class="item active">
        <img src="{{ $logo }}" alt="image" width="300px" height="300px">
    </div>
</div>


<!-- Title Id Field -->
<div class="form-group">
    {!! Form::label('reg', 'Registration Number:') !!}
    <p>{{ @$userProfiles->payments->reference }}</p>
</div>

<!-- Title Id Field -->
<div class="form-group">
    {!! Form::label('title_id', 'Title:') !!}
    <p>{{ $userProfiles->title }}</p>
</div>

<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{{ $userProfiles->first_name }}</p>
</div>

<!-- Other Name Field -->
<div class="form-group">
    {!! Form::label('other_name', 'Other Name:') !!}
    <p>{{ $userProfiles->other_name }}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{{ $userProfiles->last_name }}</p>
</div>

<!-- Sex Field -->
<div class="form-group">
    {!! Form::label('sex', 'Sex:') !!}
    <p>{{ $userProfiles->sex }}</p>
</div>

<!-- Dob Field -->
<div class="form-group">
    {!! Form::label('dob', 'Dob:') !!}
    <p>{{ $userProfiles->dob }}</p>
</div>

<!-- Marital Status Field -->
<div class="form-group">
    {!! Form::label('marital_status', 'Marital Status:') !!}
    <p>{{ $userProfiles->marital_status }}</p>
</div>

<!-- Profession Field -->
<div class="form-group">
    {!! Form::label('profession', 'Profession:') !!}
    <p>{{ $userProfiles->profession }}</p>
</div>

<!-- Employer Field -->
<div class="form-group">
    {!! Form::label('employer', 'Employer:') !!}
    <p>{{ $userProfiles->employer }}</p>
</div>

<!-- Home Address Field -->
<div class="form-group">
    {!! Form::label('home_address', 'Home Address:') !!}
    <p>{{ $userProfiles->home_address }}</p>
</div>

<!-- Office Address Field -->
<div class="form-group">
    {!! Form::label('office_address', 'Office Address:') !!}
    <p>{{ $userProfiles->office_address }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $userProfiles->email }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $userProfiles->phone }}</p>
</div>


<div class="form-group">
    {!! Form::label('next_of_kin', 'Next of Kin:') !!}
    <p>{{ $userProfiles->next_of_kin }}</p>
</div>

<div class="form-group">
    {!! Form::label('next_of_kin_phone', 'Next of Kin Phone:') !!}
    <p>{{ $userProfiles->next_of_kin_phone }}</p>
</div>

