<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form</title>


    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Tangerine">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset("img/logo.png")}}">

    <!-- Font Icon -->
    {{--<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('fonts/material-icon/css/material-design-iconic-font.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('vendor/nouislider/nouislider.min.css')}}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- Style by Juhla -->
    <link rel="stylesheet" href="{{asset('css/style2.css')}}">




</head>

<body>

<div class="main">

    <div class="container">

        {{--<div>--}}
            {{--<img src="{{ asset('img/logo.jpeg') }}" style="height: 180px;width: 180px"/>--}}
        {{--</div>--}}
        {!! Form::open(['route' => 'userProfiles.store','enctype'=>"multipart/form-data", 'method'=>"POST",'id'=>"signup-form",'class'=>"signup-form"]) !!}
        <div >



            @include('flash::message')
            <h3>Accept Terms</h3>
            <fieldset>
                <div>
                    <img src="{{ asset('img/logo.jpeg') }}" style="height: 120px;width: 120px"/>
                </div>
                <h2 style="font-size: 26px">Lekki Muslim Ummah - Itikaf 2020 Registration Portal</h2>
                {{--@include('user_profiles.steps.confirm')--}}
                {{--@include('user_profiles.steps.other-info')--}}
                {{--@include('user_profiles.steps.personal-info')--}}
                @include('user_profiles.steps.terms-main')
            </fieldset>

            <h3>Personal Information</h3>
            <fieldset>
                <div>
                    <img src="{{ asset('img/logo.jpeg') }}" style="height: 120px;width: 120px"/>
                </div>
                <h2 style="font-size: 26px">Lekki Muslim Ummah - Itikaf 2020 Registration Portal</h2>
                @include('user_profiles.steps.personal-info')
            </fieldset>

            <h3>Other Information</h3>
            <fieldset>
                <div>
                    <img src="{{ asset('img/logo.jpeg') }}" style="height: 120px;width: 120px"/>
                </div>
                <h2 style="font-size: 26px">Lekki Muslim Ummah - Itikaf 2020 Registration Portal</h2>
                @include('user_profiles.steps.other-info')
            </fieldset>


            <h3>Confirm Your Information</h3>
            <fieldset>
                <div>
                    <img src="{{ asset('img/logo.jpeg') }}" style="height: 120px;width: 120px"/>
                </div>
                <h2 style="font-size: 26px">Lekki Muslim Ummah - Itikaf 2020 Registration Portal</h2>
                @include('user_profiles.steps.confirm')
            </fieldset>
        </div>
        {!! Form::close() !!}
    </div>

</div>

<!-- JS -->

<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('vendor/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script src="{{asset('vendor/jquery-steps/jquery.steps.min.js')}}"></script>
<script src="{{asset('vendor/minimalist-picker/dobpicker.js')}}"></script>
<script src="{{asset('vendor/nouislider/nouislider.min.js')}}"></script>
<script src="{{asset('vendor/wnumb/wNumb.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{asset('bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
@include('user_profiles.steps.main-reg')
<script>
    $(function () {
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
        });
    })
</script>
</body>

</html>
