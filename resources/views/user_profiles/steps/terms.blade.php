<section>
    <div class="inner">


        <div class="form-row">
            <iframe src="{{url('terms')}}" height="300px" class="form-holder form-holder-2"></iframe>
        </div>


        <div class="form-group {{ $errors->has('sex') ? 'has-error' : ''}}">
            <div class="form-row">
                <div class="form-holder form-holder-2">
                    <label for="card-type">Gender</label>
                    <select name="sex" id="sex" required class="form-control">
                        <option value="">Pick a gender...</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
            </div>
            {!! $errors->first('sex', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-row">
            &nbsp;&nbsp;&nbsp;
            <input type="checkbox" class="form-control" id="terms" required name="terms">
            <label for="terms">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><strong>Accept Terms *</strong></span></label>
        </div>

    </div>
</section>
