<div class="fieldset-content">

    <div class="form-group">
        {!! Form::label('passport', 'Upload Picture', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::file('passport',  null, ['class' => 'form-control', 'placeholder' => 'Upload Picture']) !!}
            {!! $errors->first('passport', '<p class="help-block">:message</p>') !!}
        </div>

    </div>

    <div class="form-group  {{ $errors->has('title') ? 'has-error' : ''}}">
        {!! Form::label('title', 'Title', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::select('title', $titles, null, ['id'=>'title','class' => 'select-text', 'placeholder' => 'Pick a Title']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>

    </div>

    <div class="form-group">
        <label for="email" class="form-label">Email</label>
        <input type="email" name="email" id="email" />
        <span class="text-input">Example  :<span>  Jeff@gmail.com</span></span>
    </div>

    <div class="form-group">
        {!! Form::label('first_name', 'First Name', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('first_name',  null, ['id'=>'first_name','class' => 'form-control', 'placeholder' => 'First Name']) !!}
            {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
        </div>

    </div>

    <div class="form-group  {{ $errors->has('other_name') ? 'has-error' : ''}}">
        {!! Form::label('other_name', 'Other Name', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('other_name',  null, ['id'=>'other_name','class' => 'form-control', 'placeholder' => 'Other Name']) !!}
            {!! $errors->first('other_name', '<p class="help-block">:message</p>') !!}
        </div>

    </div>


    <div class="form-group  {{ $errors->has('last_name') ? 'has-error' : ''}}">
        {!! Form::label('last_name', 'Last Name', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('last_name',  null, ['id'=>'last_name','class' => 'form-control', 'placeholder' => 'Last Name']) !!}
            {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
        </div>

    </div>

    <div class="form-date">
        <label for="birth_date" class="form-label">Date of Birth</label>
        <div class="form-date-group">
            <div class="form-date-item">
                <select id="birth_date" name="birth_date"></select>
                <span class="text-input">DD</span>
            </div>
            <div class="form-date-item">
                <select id="birth_month" name="birth_month"></select>
                <span class="text-input">MM</span>
            </div>
            <div class="form-date-item">
                <select id="birth_year" name="birth_year"></select>
                <span class="text-input">YYYY</span>
            </div>
        </div>
    </div>

    {{--<div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">--}}
    {{--{!! Form::label('dob', 'Date of Birth', ['class' => 'col-sm-4 control-label']) !!}--}}
    {{--<div class="col-sm-10">--}}
    {{--{!! Form::text('dob', null, ['class' => 'form-control datepicker']) !!}--}}
    {{--<span class="input-group-addon">--}}
    {{--<span class="glyphicon glyphicon-calendar"></span>--}}
    {{--</span>--}}
    {{--{!! $errors->first('dob', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}
    {{--</div>--}}


    <div class="form-group  {{ $errors->has('marital_status') ? 'has-error' : ''}}">
        {!! Form::label('marital_status', 'Marital Status', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::select('marital_status', $marital, null, ['class' => 'select-text form-control', 'placeholder' => 'Pick a Status']) !!}
            {!! $errors->first('marital_status', '<p class="help-block">:message</p>') !!}
        </div>

    </div>


</div>
