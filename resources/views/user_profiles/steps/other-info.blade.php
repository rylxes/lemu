<div class="fieldset-content">


    <div class="form-group  {{ $errors->has('profession') ? 'has-error' : ''}}">
        {!! Form::label('profession', 'Profession', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('profession',  null, ['id'=>'profession','class' => 'form-control', 'placeholder' => 'Profession']) !!}
            {!! $errors->first('profession', '<p class="help-block">:message</p>') !!}
        </div>

    </div>

    <div class="form-group  {{ $errors->has('employer') ? 'has-error' : ''}}">
        {!! Form::label('employer', 'Employer', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('employer',  null, ['id'=>'employer','class' => 'form-control', 'placeholder' => 'Employer']) !!}
            {!! $errors->first('employer', '<p class="help-block">:message</p>') !!}
        </div>

    </div>


    <div class="form-group  {{ $errors->has('home_address') ? 'has-error' : ''}}">
        {!! Form::label('home_address', 'Home Address', ['class' => 'col-sm-4 control-label']) !!}
        <br/>
        <div class="col-sm-10">
            {!! Form::textarea('home_address',  null, ['id'=>'home_address','class' => 'form-control', 'placeholder' => 'Home Address','rows' => 3, 'cols' => 35]) !!}
            {!! $errors->first('home_address', '<p class="help-block">:message</p>') !!}
        </div>

    </div>


    <div class="form-group  {{ $errors->has('office_address') ? 'has-error' : ''}}">
        {!! Form::label('office_address', 'Office Address', ['class' => 'col-sm-4 control-label']) !!}
        <br/>
        <div class="col-sm-10">
            {!! Form::textarea('office_address',  null, ['id'=>'office_address','class' => 'form-control', 'placeholder' => 'Office Address','rows' => 3, 'cols' => 35]) !!}
            {!! $errors->first('office_address', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
    <div class="form-group  {{ $errors->has('email') ? 'has-error' : ''}}">
        {!! Form::label('email', 'Email', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::email('email',  null, ['id'=>'email','class' => 'form-control', 'placeholder' => 'Email']) !!}
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>

    </div>

    <div class="form-group  {{ $errors->has('phone') ? 'has-error' : ''}}">
        {!! Form::label('phone', 'Phone', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('phone',  null, ['id'=>'phone','class' => 'form-control', 'placeholder' => 'Phone']) !!}
            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
        </div>

    </div>


    <div class="form-group  {{ $errors->has('next_of_kin') ? 'has-error' : ''}}">
        {!! Form::label('next_of_kin', 'Next of Kin', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('next_of_kin',  null, ['id'=>'next_of_kin','class' => 'form-control', 'placeholder' => 'Next of Kin']) !!}
            {!! $errors->first('next_of_kin', '<p class="help-block">:message</p>') !!}
        </div>

    </div>


    <div class="form-group  {{ $errors->has('next_of_kin_phone') ? 'has-error' : ''}}">
        {!! Form::label('next_of_kin_phone', 'Next of Kin Phone', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('next_of_kin_phone',  null, ['id'=>'next_of_kin_phone','class' => 'form-control', 'placeholder' => 'Next of Kin Phone']) !!}
            {!! $errors->first('next_of_kin_phone', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
</div>
