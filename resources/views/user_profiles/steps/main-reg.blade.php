<script>


    (function ($) {


        var form = $("#signup-form");
        $.validator.addMethod('filesize', function(value, element, param) {
            // param = size (en bytes)
            // element = element to validate (<input>)
            // value = value of the element (file name)
            return this.optional(element) || (element.files[0].size <= param)
        });
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.after(error);
            },
            onfocusout: function (element) {
                $(element).valid();
            },
            rules: {
                email: {
                    email: true,
                    required: true,

                },
                passport: {
                    required: true,
                    extension: "jpeg|png|jpg|gif|svg",
                    filesize: 2000000
                },
                sex: {
                    required: true,
                },
                title: {
                    required: true,
                },
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                terms: {
                    required: true,
                },
                birth_date: {
                    required: true,
                },
                birth_month: {
                    required: true,
                },
                birth_year: {
                    required: true,
                },
                marital_status: {
                    required: true,
                },
                profession: {
                    required: true,
                },
                employer: {
                    required: true,
                },
                home_address: {
                    required: true,
                },
                office_address: {
                    required: true,
                },
                phone: {
                    required: true,
                    minlength:11,
                    maxlength:11,
                },
                next_of_kin: {
                    required: true,
                },
                next_of_kin_phone: {
                    required: true,
                    minlength:11,
                    maxlength:11,
                    digits:true
                },
            },
            messages: {
                email: {
                    email: "Valid Email is Required",
                    required: "Email is Required",
                },
                profession: {
                    required: "Your Profession is Required"
                },
                employer: {
                    required: "Employer is Required"
                },
                home_address: {
                    required: "Home Address is Required"
                },
                office_address: {
                    required: "Office Address is Required"
                },
                phone: {
                    required: "Your Phone Number is Required"
                },
                next_of_kin: {
                    required: "Next of kin is Required"
                },
                next_of_kin_phone: {
                    required: "Next of kin's Phone Number is Required"
                },
                terms: {
                    required: "You must Accept Terms"
                },
                passport: {
                    required: "You must upload a passport",
                    extension: "You must upload a valid picture",
                    filesize:" file size must be less than 2 Mb.",
                },
                sex: {
                    required: "Select a Gender"
                },
                title: {
                    required: "Select a Title"
                },
                first_name: {
                    required: "First Name is Required"
                },
                last_name: {
                    required: "Last Name is Required"
                },
                birth_date: {
                    required: "Select a Date"
                },
                birth_month: {
                    required: "Select a Date"
                },
                birth_year: {
                    required: "Select a Date"
                },
                marital_status: {
                    required: "Select a Marital Status"
                },
            }
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "fieldset",
            enableAllSteps: false,
            //saveState: true,
            autoFocus: true,
            transitionEffect: "fade",
            transitionEffectSpeed: 500,
            stepsOrientation: "vertical",
            titleTemplate: '<div class="title"><span class="step-number">#index#</span><span class="step-text">#title#</span></div>',
            labels: {
                previous: 'Previous',
                next: 'Next',
                finish: 'Finish',
                current: ''
            },
            onInit: function(event, currentIndex) {
                if(currentIndex == 0)
                    $('.wizard .content').css("height", '68em');
            },
            onStepChanging: function (event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                var move = false;
                if (currentIndex === 0) {

                    form.parent().parent().parent().append('<div class="footer footer-' + currentIndex + '"></div>');
                    move = false;

                    if (form.valid()) {
                        $.ajax({
                            type: 'GET',
                            url: "{{url('getCount')}}",
                            data: {'gender': $("#sex").val()},
                            async: false,
                            cache: false,
                            timeout: 30000,
                            contentType: "application/json",
                            dataType: 'json',
                            success: function (data) {
                                move = data.type;
                                return true;
                            },
                            error: function (data) {
                                move = false;
                                return true;
                            }
                        });

                        if (!move) {
                            Swal.fire({
                                title: 'Error!',
                                text: 'We are fully SOLD OUT for the selected GENDER. There are no more open Itikaf slots for your gender. You cannot continue',
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            })

                        }
                    }

                    return form.valid() && move;
                }
                if (currentIndex === 1) {

                    form.parent().parent().parent().find('.footer').removeClass('footer-0').addClass('footer-' + currentIndex + '');
                }
                if (currentIndex === 2) {
                    form.parent().parent().parent().find('.footer').removeClass('footer-1').addClass('footer-' + currentIndex + '');
                }
                if (currentIndex === 3) {
                    form.parent().parent().parent().find('.footer').removeClass('footer-2').addClass('footer-' + currentIndex + '');
                }

                if (currentIndex > newIndex) {
                    return true;
                }





                var cardnumber = $('#card-number').val();
                var cvc = $('#cvc').val();
                var month = $('#month').val();
                var year = $('#year').val();

                $('#sex-val').text($('#sex').val());
                $('#email-val').text($('#email').val());
                $('#title-val').text($('#title').val());
                $('#first_name-val').text($('#first_name').val());
                $('#other_name-val').text($('#other_name').val());
                $('#last_name-val').text($('#last_name').val());
                $('#birth_date-val').text($('#birth_date').val());
                $('#birth_month-val').text($('#birth_month').val());
                $('#birth_year-val').text($('#birth_year').val());
                $('#marital_status-val').text($('#marital_status').val());
                $('#profession-val').text($('#profession').val());
                $('#employer-val').text($('#employer').val());
                $('#home_address-val').text($('#home_address').val());
                $('#office_address-val').text($('#office_address').val());
                $('#phone-val').text($('#phone').val());
                $('#next_of_kin-val').text($('#next_of_kin').val());
                $('#next_of_kin_phone-val').text($('#next_of_kin_phone').val());



                $('#card-number-val').text(cardnumber);
                $('#cvc-val').text(cvc);
                $('#month-val').text(month);
                $('#year-val').text(year);



                if (currentIndex < newIndex) {
                    // To remove error styles
                    form.find(".body:eq(" + newIndex + ") label.error").remove();
                    form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                }

                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex) {

                form.submit();

            },
            onStepChanged: function (event, currentIndex, priorIndex) {
                console.log(currentIndex);
                if (currentIndex === 4) {
                    //if last step
                    //remove default #finish button
                    $('#wizard').find('a[href="#finish"]').remove();
                    //append a submit type button
                    $('#wizard .actions li:last-child').append('<button type="submit" id="submit" class="btn-large"><span class="fa fa-chevron-right"></span></button>');
                }
                return true;
            }
        });

        jQuery.extend(jQuery.validator.messages, {
            required: "",
            remote: "",
            email: "",
            url: "",
            date: "",
            dateISO: "",
            number: "",
            digits: "",
            creditcard: "",
            equalTo: ""
        });

        $.dobPicker({
            daySelector: '#birth_date',
            monthSelector: '#birth_month',
            yearSelector: '#birth_year',
            dayDefault: '',
            monthDefault: '',
            yearDefault: '',
            minimumAge: 10,
            maximumAge: 120
        });
    })(jQuery);
</script>
