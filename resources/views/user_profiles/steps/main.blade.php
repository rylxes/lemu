<script>
    $(function () {

        async function foo() {
            return await $.ajax({
                url: "{{url('getCount')}}",
                data: {'gender': $("#sex").val()},
                type: "GET",
                dataType: 'json',
                success: function (e) {
                    return e.type;
                },
                error: function (e) {

                }
            });
        }


        $("#form-register").validate({
            rules: {
                password: {
                    required: true,
                },
                confirm_password: {
                    equalTo: "#password"
                },
                terms: {
                    required: true,
                },
                sex: {
                    required: true,
                }


            },
            messages: {
                username: {
                    required: "Please provide an username"
                },
                sex: {
                    required: "Select a Gender"
                },
                terms: {
                    required: "Accept Terms to continue"
                },
                email: {
                    required: "Please provide an email"
                },
                password: {
                    required: "Please provide a password"
                },
                confirm_password: {
                    required: "Please provide a password",
                    equalTo: "Please enter the same password"
                }
            }
        });

        var form = $("#form-total").show();
        form.steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "fade",
            //   enableAllSteps: true,
            autoFocus: true,
            transitionEffectSpeed: 500,
            titleTemplate: '<div class="title">#title#</div>',
            labels: {
                previous: 'Back',
                next: '<i class="zmdi zmdi-arrow-right"></i>',
                finish: '<i class="zmdi zmdi-arrow-right"></i>',
                current: ''
            },
            onStepChanged: function (event, currentIndex, priorIndex) {

                // console.log(priorIndex)
            },
            onStepChanging: function (event, currentIndex, newIndex) {

                var move = false;
                if (currentIndex === 0) {
                    move = false;
                    $.ajax({
                        type: 'GET',
                        url: "{{url('getCount')}}",
                        data: {'gender': $("#sex").val()},
                        async: false,
                        cache: false,
                        timeout: 30000,
                        contentType: "application/json",
                        dataType: 'json',
                        success: function (data) {
                            move = data.type;
                            return true;
                        },
                        error: function (data) {
                            move = false;
                            return true;
                        }
                    });



                    if(!move){
                        $(".body:eq(" + currentIndex + ") label.error", form).add();
                        $(".body:eq(" + currentIndex + ") .error", form).addClass("error");
                    }
                    return move;


                }


                if (currentIndex > newIndex) {
                    return true;
                }
                var username = $('#username').val();



                var email = $('#email').val();
                var cardtype = $('#card-type').val();
                var cardnumber = $('#card-number').val();
                var cvc = $('#cvc').val();
                var month = $('#month').val();
                var year = $('#year').val();

                $('#username-val').text(username);
                $('#email-val').text(email);
                $('#card-type-val').text(cardtype);
                $('#card-number-val').text(cardnumber);
                $('#cvc-val').text(cvc);
                $('#month-val').text(month);
                $('#year-val').text(year);


                if (currentIndex < newIndex) {
                    // To remove error styles
                    form.find(".body:eq(" + newIndex + ") label.error").remove();
                    form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                }

                $("#form-register").validate().settings.ignore = ":disabled,:hidden";
                return $("#form-register").valid();
            }
        });
    });
</script>
