<div class="fieldset-content">
    <br/><br/>

    <div class="form-group">
        <iframe src="{{url('terms')}}" height="600px" width="100%"></iframe>
    </div>


    <div class="form-group">
        <select class="form-control" name="sex" id="sex" required>
            <option value="">Pick a Gender</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
    </div>

    <div class="form-group">
        <table class="col-sm-10">
            <tr>
                <td>
                    <input style="width: 10em" type="checkbox" id="terms" required name="terms">
                </td>
                <td>
                    <label for="terms"><span>Accept Terms *</span></label>
                </td>
            </tr>
        </table>

    </div>

</div>
