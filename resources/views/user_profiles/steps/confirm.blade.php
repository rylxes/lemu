<br/><br/>
<div class="fieldset-content">
    <div class="">
        <table class="">
            <tbody>
            <tr style="height: 40px">
                <th>Gender:</th>
                <td id="sex-val"></td>
            </tr>
            <tr style="height: 40px">
                <th>Email Address:</th>
                <td id="email-val"></td>
            </tr>
            <tr style="height: 40px">
                <th>Title:</th>
                <td id="title-val"></td>
            </tr>
            <tr style="height: 40px">
                <th>First Name:</th>
                <td id="first_name-val"></td>
            </tr>
            <tr style="height: 40px">
                <th>Other Name:</th>
                <td id="other_name-val"></td>
            </tr>
            <tr style="height: 40px">
                <th>Last Name:</th>
                <td id="last_name-val"></td>
            </tr>
            <tr style="height: 40px">
                <th>Date of Birth:</th>
                <td><span id="birth_date-val"></span> - <span id="birth_month-val"></span> - <span
                        id="birth_year-val"></span></td>
            </tr>
            <tr style="height: 40px">
                <th>Marital Status:</th>
                <td id="marital_status-val"></td>
            </tr>

            <tr style="height: 40px">
                <th>Profession:</th>
                <td id="profession-val"></td>
            </tr>


            <tr style="height: 40px">
                <th>Employer:</th>
                <td id="employer-val"></td>
            </tr>


            <tr style="height: 40px">
                <th>Home Address:</th>
                <td id="home_address-val"></td>
            </tr>


            <tr style="height: 40px">
                <th>Office Address:</th>
                <td id="office_address-val"></td>
            </tr>

            <tr style="height: 40px">
                <th>Phone:</th>
                <td id="phone-val"></td>
            </tr>


            <tr style="height: 40px">
                <th>Next of Kin:</th>
                <td id="next_of_kin-val"></td>
            </tr>

            <tr style="height: 40px">
                <th>Next of Kin Phone:</th>
                <td id="next_of_kin_phone-val"></td>
            </tr>



            </tbody>
        </table>
    </div>
</div>
