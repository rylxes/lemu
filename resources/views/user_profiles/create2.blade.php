@extends('layouts.unauth')

@section('content')
    <span>
						<img src="{{ asset('img/logo.png') }}" alt="" class="wheel-img"
                             style="padding-left: 30px"/>
					</span>
    <div class="container-fluid">
        <div class="animated fadeIn">

            <div class="row">

                <div class="col-lg-8">
                    @include('flash::message')
                    <div class="card">
                        <div class="card-header">
                            <strong><h3>Lekki Muslim Ummah - Itikaf 2020 Registrations</h3></strong>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'userProfiles.store','enctype'=>"multipart/form-data",'id'=>"payment_form"]) !!}

                            @include('user_profiles.fields')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('stylesheet')
    <link rel="stylesheet" href="{{asset('bootstrap-datepicker/css/bootstrap-datepicker3.css')}}">
@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{asset('bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script>
        $(function () {
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
            });
        })
    </script>
@endsection

