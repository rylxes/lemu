<li class="nav-item {{ Request::is('userProfiles*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('userProfiles.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Registered (Paid) Users</span>
    </a>
</li>

<li class="nav-item {{ Request::is('userProfiles*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ url('userProfiles/unpaid') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Un Paid Users</span>
    </a>
</li>

<li class="nav-item {{ Request::is('payments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('payments.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Payments</span>
    </a>
</li>
<li class="nav-item {{ Request::is('settings*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('settings.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Settings</span>
    </a>
</li>
