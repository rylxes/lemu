<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav" style="width: 300px">
            @include('layouts.menu')
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
