<!-- Model Id Field -->
<div class="form-group">
    {!! Form::label('model_id', 'Model Id:') !!}
    <p>{{ $modelSettings->model_id }}</p>
</div>

<!-- Model Type Field -->
<div class="form-group">
    {!! Form::label('model_type', 'Model Type:') !!}
    <p>{{ $modelSettings->model_type }}</p>
</div>

<!-- Settings Field -->
<div class="form-group">
    {!! Form::label('settings', 'Settings:') !!}
    <p>{{ $modelSettings->settings }}</p>
</div>

