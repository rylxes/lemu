<!-- Profile Id Field -->
<div class="form-group">
    {!! Form::label('profile_id', 'Profile Id:') !!}
    <p>{{ $payment->profile_id }}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $payment->amount }}</p>
</div>

<!-- Reference Field -->
<div class="form-group">
    {!! Form::label('reference', 'Reference:') !!}
    <p>{{ $payment->reference }}</p>
</div>

<!-- Meta Field -->
<div class="form-group">
    {!! Form::label('meta', 'Meta:') !!}
    <p>{{ $payment->meta }}</p>
</div>

