<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!-- NAME: GDPR SUBSCRIBER ALERT -->
    <!--[if gte mso 15]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LEMU</title>

    <style type="text/css">
        p {
            margin: 10px 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
        }

        h1, h2, h3, h4, h5, h6 {
            display: block;
            margin: 0;
            padding: 0;
        }

        img, a img {
            border: 0;
            height: auto;
            outline: none;
            text-decoration: none;
        }

        body, #bodyTable, #bodyCell {
            height: 100%;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .mcnPreviewText {
            display: none !important;
        }

        #outlook a {
            padding: 0;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        p, a, li, td, blockquote {
            mso-line-height-rule: exactly;
        }

        a[href^=tel], a[href^=sms] {
            color: inherit;
            cursor: default;
            text-decoration: none;
        }

        p, a, li, td, body, table, blockquote {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass td, .ExternalClass div, .ExternalClass span, .ExternalClass font {
            line-height: 100%;
        }

        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        .templateContainer {
            max-width: 600px !important;
        }

        a.mcnButton {
            display: block;
        }

        .mcnImage, .mcnRetinaImage {
            vertical-align: bottom;
        }

        .mcnTextContent {
            word-break: break-word;
        }

        .mcnTextContent img {
            height: auto !important;
        }

        .mcnDividerBlock {
            table-layout: fixed !important;
        }

        /*
         Page

        
        Heading 1
            @style heading 1
            */
        h1 {
            /*@editable*/
            color: #222222;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 40px;
            /*@editable*/
            font-style: normal;
            /*@editable*/
            font-weight: bold;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            letter-spacing: normal;
            /*@editable*/
            text-align: center;
        }

        /*
         Page

        
        Heading 2
            @style heading 2
            */
        h2 {
            /*@editable*/
            color: #222222;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 34px;
            /*@editable*/
            font-style: normal;
            /*@editable*/
            font-weight: bold;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            letter-spacing: normal;
            /*@editable*/
            text-align: left;
        }

        /*
         Page

        
        Heading 3
            @style heading 3
            */
        h3 {
            /*@editable*/
            color: #444444;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 22px;
            /*@editable*/
            font-style: normal;
            /*@editable*/
            font-weight: bold;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            letter-spacing: normal;
            /*@editable*/
            text-align: left;
        }

        /*
         Page

        
        Heading 4
            @style heading 4
            */
        h4 {
            /*@editable*/
            color: #999999;
            /*@editable*/
            font-family: Georgia;
            /*@editable*/
            font-size: 20px;
            /*@editable*/
            font-style: italic;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            line-height: 125%;
            /*@editable*/
            letter-spacing: normal;
            /*@editable*/
            text-align: left;
        }

        /*
         Header

        
        Header Container Style
            */
        #templateHeader {
            /*@editable*/
            background-color: #F2F2F2;
            /*@editable*/
            background-image: none;
            /*@editable*/
            background-repeat: no-repeat;
            /*@editable*/
            background-position: center;
            /*@editable*/
            background-size: cover;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
            /*@editable*/
            padding-top: 36px;
            /*@editable*/
            padding-bottom: 0;
        }

        /*
         Header

        
        Header Interior Style
            */
        .headerContainer {
            /*@editable*/
            background-color: #FFFFFF;
            /*@editable*/
            background-image: none;
            /*@editable*/
            background-repeat: no-repeat;
            /*@editable*/
            background-position: center;
            /*@editable*/
            background-size: cover;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
            /*@editable*/
            padding-top: 45px;
            /*@editable*/
            padding-bottom: 45px;
        }

        /*
         Header

        
        Header Text
            */
        .headerContainer .mcnTextContent, .headerContainer .mcnTextContent p {
            /*@editable*/
            color: #808080;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 16px;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            text-align: left;
        }

        /*
         Header

        
        Header Link
            */
        .headerContainer .mcnTextContent a, .headerContainer .mcnTextContent p a {
            /*@editable*/
            color: #007E9E;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
        }

        /*
         Body

        
        Body Container Style
            */
        #templateBody {
            /*@editable*/
            background-color: #F2F2F2;
            /*@editable*/
            background-image: none;
            /*@editable*/
            background-repeat: no-repeat;
            /*@editable*/
            background-position: center;
            /*@editable*/
            background-size: cover;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
            /*@editable*/
            padding-top: 0;
            /*@editable*/
            padding-bottom: 0;
        }

        /*
         Body

        
        Body Interior Style
            */
        .bodyContainer {
            /*@editable*/
            background-color: #FFFFFF;
            /*@editable*/
            background-image: none;
            /*@editable*/
            background-repeat: no-repeat;
            /*@editable*/
            background-position: center;
            /*@editable*/
            background-size: cover;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
            /*@editable*/
            padding-top: 0;
            /*@editable*/
            padding-bottom: 45px;
        }

        /*
         Body

        
        Body Text
            */
        .bodyContainer .mcnTextContent, .bodyContainer .mcnTextContent p {
            /*@editable*/
            color: #808080;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 16px;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            text-align: left;
        }

        /*
         Body

        
        Body Link
            */
        .bodyContainer .mcnTextContent a, .bodyContainer .mcnTextContent p a {
            /*@editable*/
            color: #007E9E;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
        }

        /*
         Footer

        
        Footer Style
            */
        #templateFooter {
            /*@editable*/
            background-color: #F2F2F2;
            /*@editable*/
            background-image: none;
            /*@editable*/
            background-repeat: no-repeat;
            /*@editable*/
            background-position: center;
            /*@editable*/
            background-size: cover;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
            /*@editable*/
            padding-top: 0;
            /*@editable*/
            padding-bottom: 36px;
        }

        /*
         Footer

        
        Footer Interior Style
            */
        .footerContainer {
            /*@editable*/
            background-color: #333333;
            /*@editable*/
            background-image: none;
            /*@editable*/
            background-repeat: no-repeat;
            /*@editable*/
            background-position: center;
            /*@editable*/
            background-size: cover;
            /*@editable*/
            border-top: 0;
            /*@editable*/
            border-bottom: 0;
            /*@editable*/
            padding-top: 45px;
            /*@editable*/
            padding-bottom: 45px;
        }

        /*
         Footer

        
        Footer Text
            */
        .footerContainer .mcnTextContent, .footerContainer .mcnTextContent p {
            /*@editable*/
            color: #FFFFFF;
            /*@editable*/
            font-family: Helvetica;
            /*@editable*/
            font-size: 12px;
            /*@editable*/
            line-height: 150%;
            /*@editable*/
            text-align: center;
        }

        /*
         Footer

        
        Footer Link
            */
        .footerContainer .mcnTextContent a, .footerContainer .mcnTextContent p a {
            /*@editable*/
            color: #FFFFFF;
            /*@editable*/
            font-weight: normal;
            /*@editable*/
            text-decoration: underline;
        }

        @media only screen and (min-width: 768px) {
            .templateContainer {
                width: 600px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            body, table, td, p, a, li, blockquote {
                -webkit-text-size-adjust: none !important;
            }

        }

        @media only screen and (max-width: 480px) {
            body {
                width: 100% !important;
                min-width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnRetinaImage {
                max-width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImage {
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnCartContainer, .mcnCaptionTopContent, .mcnRecContentContainer, .mcnCaptionBottomContent, .mcnTextContentContainer, .mcnBoxedTextContentContainer, .mcnImageGroupContentContainer, .mcnCaptionLeftTextContentContainer, .mcnCaptionRightTextContentContainer, .mcnCaptionLeftImageContentContainer, .mcnCaptionRightImageContentContainer, .mcnImageCardLeftTextContentContainer, .mcnImageCardRightTextContentContainer, .mcnImageCardLeftImageContentContainer, .mcnImageCardRightImageContentContainer {
                max-width: 100% !important;
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnBoxedTextContentContainer {
                min-width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageGroupContent {
                padding: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnCaptionLeftContentOuter .mcnTextContent, .mcnCaptionRightContentOuter .mcnTextContent {
                padding-top: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageCardTopImageContent, .mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent, .mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent {
                padding-top: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageCardBottomImageContent {
                padding-bottom: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageGroupBlockInner {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageGroupBlockOuter {
                padding-top: 9px !important;
                padding-bottom: 9px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnTextContent, .mcnBoxedTextContentColumn {
                padding-right: 18px !important;
                padding-left: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcnImageCardLeftImageContent, .mcnImageCardRightImageContent {
                padding-right: 18px !important;
                padding-bottom: 0 !important;
                padding-left: 18px !important;
            }

        }

        @media only screen and (max-width: 480px) {
            .mcpreview-image-uploader {
                display: none !important;
                width: 100% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            /*
             Mobile Styles

         Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
            h1 {
                /*@editable*/
                font-size: 30px !important;
                /*@editable*/
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            /*
             Mobile Styles

         Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
            h2 {
                /*@editable*/
                font-size: 26px !important;
                /*@editable*/
                line-height: 125% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            /*
             Mobile Styles

         Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
            h3 {
                /*@editable*/
                font-size: 20px !important;
                /*@editable*/
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            /*
             Mobile Styles

         Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
            h4 {
                /*@editable*/
                font-size: 18px !important;
                /*@editable*/
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            /*
             Mobile Styles

         Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
            .mcnBoxedTextContentContainer .mcnTextContent, .mcnBoxedTextContentContainer .mcnTextContent p {
                /*@editable*/
                font-size: 14px !important;
                /*@editable*/
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            /*
             Mobile Styles

         Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
            .headerContainer .mcnTextContent, .headerContainer .mcnTextContent p {
                /*@editable*/
                font-size: 16px !important;
                /*@editable*/
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            /*
             Mobile Styles

         Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
            .bodyContainer .mcnTextContent, .bodyContainer .mcnTextContent p {
                /*@editable*/
                font-size: 16px !important;
                /*@editable*/
                line-height: 150% !important;
            }

        }

        @media only screen and (max-width: 480px) {
            /*
             Mobile Styles

         Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
            .footerContainer .mcnTextContent, .footerContainer .mcnTextContent p {
                /*@editable*/
                font-size: 14px !important;
                /*@editable*/
                line-height: 150% !important;
            }

        }</style>
</head>
<body>
<!--*|IF:MC_PREVIEW_TEXT|*-->
<!--[if !gte mso 9]><!--><span class="mcnPreviewText"
                                 style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Welcome</span>
<!--<![endif]-->
<!--*|END:IF|*-->
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" valign="top" id="templateHeader">
                            <!--[if (gte mso 9)|(IE)]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600"
                                   style="width:600px;">
                                <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                            <![endif]-->
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                   class="templateContainer">
                                <tr>
                                    <td valign="top" class="headerContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnImageBlock" style="min-width:100%;">
                                            <tbody class="mcnImageBlockOuter">
                                            <tr>
                                                <td valign="top" style="padding:9px" class="mcnImageBlockInner">
                                                    <table align="left" width="100%" border="0" cellpadding="0"
                                                           cellspacing="0" class="mcnImageContentContainer"
                                                           style="min-width:100%;">
                                                        <tbody>
                                                        <tr>
                                                            <td class="mcnImageContent" valign="top"
                                                                style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;">


                                                                {{--<img align="center" alt=""--}}
                                                                     {{--src="https://gallery.mailchimp.com/1cd2c15ff4eeaba8668f22ea9/images/a8f9b410-a1e4-4730-b507-ba297fad0ff0.jpeg"--}}
                                                                     {{--width="208.68"--}}
                                                                     {{--style="max-width:1040px; padding-bottom: 0; display: inline !important; vertical-align: bottom;"--}}
                                                                     {{--class="mcnImage">--}}


                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnTextBlock" style="min-width:100%;">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                    <!--[if mso]>
                                                    <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                           width="100%" style="width:100%;">
                                                        <tr>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    <td valign="top" width="600" style="width:600px;">
                                                    <![endif]-->
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                           style="max-width:100%; min-width:100%;" width="100%"
                                                           class="mcnTextContentContainer">
                                                        <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent"
                                                                style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">


                                                                <div style="text-align: center;">&nbsp;</div>

                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if mso]>
                                                    </td>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    </tr>
                                                    </table>
                                                    <![endif]-->
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnTextBlock" style="min-width:100%;">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                    <!--[if mso]>
                                                    <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                           width="100%" style="width:100%;">
                                                        <tr>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    <td valign="top" width="600" style="width:600px;">
                                                    <![endif]-->
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                           style="max-width:100%; min-width:100%;" width="100%"
                                                           class="mcnTextContentContainer">
                                                        <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent"
                                                                style="padding: 0px 18px 9px; font-size: 16px; line-height: 125%;">

                            <span style="font-size:14px">

                                <span style="font-family:arial,helvetica neue,helvetica,sans-serif">Registration Number : {{$userProfiles->payments->reference}}</span>
                                <br/>
                                <span
                                    style="font-family:arial,helvetica neue,helvetica,sans-serif">Dear {{$userProfiles->title }} {{$userProfiles->last_name }} {{$userProfiles->first_name}},<br>
Assalam Alaikum.<br>
Welcome to this year’s Itikaf.<br>
You have successfully been registered and confirmed as a participant in this year’s Itikaf. We look forward to welcoming you.<br>
To ensure a seamless Itikaf process, please note the following:</span></span>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if mso]>
                                                    </td>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    </tr>
                                                    </table>
                                                    <![endif]-->
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" id="templateBody">
                            <!--[if (gte mso 9)|(IE)]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600"
                                   style="width:600px;">
                                <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                            <![endif]-->
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                   class="templateContainer">
                                <tr>
                                    <td valign="top" class="bodyContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnTextBlock" style="min-width:100%;">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                    <!--[if mso]>
                                                    <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                           width="100%" style="width:100%;">
                                                        <tr>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    <td valign="top" width="600" style="width:600px;">
                                                    <![endif]-->
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                           style="max-width:100%; min-width:100%;" width="100%"
                                                           class="mcnTextContentContainer">
                                                        <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent"
                                                                style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                                <h3><span
                                                                        style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                            style="font-size:14px">&nbsp;</span><br>
<span style="font-size:16px"><strong>ACCREDITATION</strong></span></span></h3>

                                                                <ul>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">All participants will have to complete the accreditation process before access can be granted to the bed spaces within the Masjid.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Accreditation will start at 8am and end by 10pm daily.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">All participants are required to come along with a valid ID card (Driver’s License or Voters Card or Data page of International Passport or National ID Card) that corresponds with the personal details provided during the registration process.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px"><strong>Participants are required to provide evidence of registration (i.e. Confirmation Email and PAYSTACK Payment Receipt).</strong></span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Itikaf ID Cards will be produced prior to the commencement of Itikaf and distributed to participants at the Reception Desk before entry will be granted.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Bed spaces will be allocated to each participant. Participants must maintain the allocated bed spaces throughout the Itikaf period as this will reduce any inconvenience during daily routine security checks. Every participant must come with either a mattress or bed spread or mat or some other covering. Participants will not be allowed to sleep on the bare Masjid rug.</span></span>
                                                                    </li>
                                                                </ul>
                                                                <span
                                                                    style="font-family:arial,helvetica neue,helvetica,sans-serif">&nbsp;<br>
<span style="font-size:16px"><strong>ITIKAF RULES</strong></span></span>

                                                                <ul>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Participants must be tolerant of one another.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Participants should not raise their voices inside the Masjid. Participants should desist from acts that may inconvenience, distract or harm other participants.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Fights or quarrels are not allowed.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">ID Cards must be worn at all times.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Informal gatherings in groups (Males Only, Females Only or Mixed Gender Groups) around the precincts of the Masjid for parties, leisure/relaxation time and unrelated activities are not allowed, and should be completely avoided.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Phones must be switched off or put on silent mode within the Masjid.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Cooking within the precincts of the Masjid is not allowed.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Boiling rings, gas burners/cylinders, electric kettles, and pressing irons are not allowed. The items will be seized if found within the precincts of the Masjid (Arrangements will be made by the Ummah to provide hot water for bathing and drinking).</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Management reserves the right to introduce new rules during the Itikaf period and participants are bound to obey the rules as prescribed.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Management reserves the right to decamp any participant for breaching any of the rules itemised above or introduced during the Itikaf period.</span></span>
                                                                    </li>
                                                                </ul>
                                                                <span
                                                                    style="font-family:arial,helvetica neue,helvetica,sans-serif"> &nbsp;<br>
<strong>FEEDING ARRANGEMENTS</strong></span>

                                                                <ul>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Participants are not allowed to cook within the precincts of the Masjid.</span></span>
                                                                    </li>
                                                                    <li><span
                                                                            style="font-family:arial,helvetica neue,helvetica,sans-serif"><span
                                                                                style="font-size:14px">Food vendors will sell various food items during Suhoor and Iftar within the Jubril Ayinla Multipurpose Hall at affordable prices. Participants are advised to come with sufficient funds to cover their feeding needs.</span></span>
                                                                    </li>
                                                                </ul>

                                                                <h3>&nbsp;</h3>

                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if mso]>
                                                    </td>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    </tr>
                                                    </table>
                                                    <![endif]-->
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnDividerBlock" style="min-width:100%;">
                                            <tbody class="mcnDividerBlockOuter">
                                            <tr>
                                                <td class="mcnDividerBlockInner"
                                                    style="min-width: 100%; padding: 9px 18px 0px;">
                                                    <table class="mcnDividerContent" border="0" cellpadding="0"
                                                           cellspacing="0" width="100%" style="min-width:100%;">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <span></span>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--
                                                                    <td class="mcnDividerBlockInner" style="padding: 18px;">
                                                                    <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                                                    -->
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnTextBlock" style="min-width:100%;">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                    <!--[if mso]>
                                                    <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                           width="100%" style="width:100%;">
                                                        <tr>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    <td valign="top" width="600" style="width:600px;">
                                                    <![endif]-->
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                           style="max-width:100%; min-width:100%;" width="100%"
                                                           class="mcnTextContentContainer">
                                                        <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent"
                                                                style="padding: 0px 18px 9px; font-size: 16px; line-height: 125%;">

                            <span style="font-size:14px"><span
                                    style="font-family:arial,helvetica neue,helvetica,sans-serif">Participants are encouraged to research and study Itikaf Rules from authentic Islamic sources before the Itikaf commencement date. It is important that participants have this foundational knowledge to prepare them for this noble endeavour.<br>
Kindly contact Alhaji Olajide on 0803 320 2212 or Salahuddin on 0802 295 1553 or send an email to <a
                                        href="mailto:lemuitikaf@lekkicentralmosque.org">lemuitikaf@lekkicentralmosque.org </a> if you require any further clarifications.<br>
&nbsp;<br>
Ma Salam.<br>
Alhaja Shopeyin</span><br>
<strong>Chairperson, </strong><strong>Ramadan Committee</strong></span>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if mso]>
                                                    </td>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    </tr>
                                                    </table>
                                                    <![endif]-->
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" id="templateFooter">
                            <!--[if (gte mso 9)|(IE)]>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600"
                                   style="width:600px;">
                                <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                            <![endif]-->
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"
                                   class="templateContainer">
                                <tr>
                                    <td valign="top" class="footerContainer">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnTextBlock" style="min-width:100%;">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                                                    <!--[if mso]>
                                                    <table align="left" border="0" cellspacing="0" cellpadding="0"
                                                           width="100%" style="width:100%;">
                                                        <tr>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    <td valign="top" width="600" style="width:600px;">
                                                    <![endif]-->
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                                                           style="max-width:100%; min-width:100%;" width="100%"
                                                           class="mcnTextContentContainer">
                                                        <tbody>
                                                        <tr>

                                                            <td valign="top" class="mcnTextContent"
                                                                style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                                                                <em>Copyright © 2020&nbsp; LEMU, All rights
                                                                    reserved.</em><br>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--[if mso]>
                                                    </td>
                                                    <![endif]-->

                                                    <!--[if mso]>
                                                    </tr>
                                                    </table>
                                                    <![endif]-->
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!--[if (gte mso 9)|(IE)]>
                            </td>
                            </tr>
                            </table>
                            <![endif]-->
                        </td>
                    </tr>
                </table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </table>
</center>
</body>
</html>
