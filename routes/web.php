<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserProfilesController@register');
Route::get('thanks', 'UserProfilesController@thanks');
Route::get('/signup', 'UserProfilesController@signup');
Route::get('/terms', 'UserProfilesController@terms');
Route::get('/getCount', 'UserProfilesController@getCount');
Route::post('/create', 'UserProfilesController@create');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->middleware('verified');



Route::post('/pay', [
    'uses' => 'PaymentController@redirectToGateway',
    'as' => 'pay'
]);
Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');
Route::get('/payment/webhook', 'PaymentController@handleGatewayWebhook');
Route::get('userProfiles/unpaid', 'UserProfilesController@unpaid');
Route::get('userProfiles/download_images', 'UserProfilesController@download_images');
Route::resource('userProfiles', 'UserProfilesController');

Route::resource('payments', 'PaymentController');


Route::resource('modelSettings', 'ModelSettingsController');

Route::resource('settings', 'SettingsController');
