<?php

namespace App\Console\Commands;

use App\Events\AfterRegister;
use App\Models\Payment;
use App\Models\UserProfiles;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Paystack\Paystack as PayStackHack;

class cleanUpPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ps = new PayStackHack();
        $total = $ps->getTransactionTotal();
        $total = $total['total_transactions'];
        //$all = Payment::all()->count();


        // dd($all,$total);


        $date = Carbon::now(); // or whatever you're using to set it
        $start = $date->copy()->startOfDay()->toDateTimeLocalString();
        //$start = $date->copy()->startOfWeek()->toDateTimeLocalString();
        $end = $date->copy()->subMinutes(5)->toDateTimeLocalString();



        //dd($start);
        $paymentDetails = $ps->getAllTransactions(['status' => 'success', 'perPage' => $total, 'from' => $start, 'to' => $end]);
        foreach ($paymentDetails as $each) {

            $payt = Payment::where('reference', @$each['reference'])->first();
            if (empty($payt)) {
                $status = @$each['status'];
                if ($status == 'success') {
                    try {
                        $up = UserProfiles::where('reference', @$each['reference'])->first();
                        if (!empty($up)) {
                            $input = [
                                'profile_id' => $up->id,
                                'amount' => $each['amount'] / 100,
                                'reference' => $each['reference'],
                                'meta' => json_encode($each),
                            ];
                            Payment::create($input);
                            event(new AfterRegister($up));
                        }

                    } catch (\Exception $exception) {

                    }

                }
            }

        }
        //dd($input);
        $this->comment('Finished');
        //dd($paymentDetails);
    }
}
