<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Payment
 * @package App\Models
 * @version January 30, 2020, 10:15 pm UTC
 *
 * @property integer profile_id
 * @property string amount
 * @property string reference
 * @property string meta
 */
class Payment extends Model
{
    use SoftDeletes;

    public $table = 'payment';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'profile_id',
        'amount',
        'reference',
        'meta'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'profile_id' => 'integer',
        'amount' => 'string',
        'reference' => 'string',
        'meta' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'profile_id' => 'required',
        'amount' => 'required',
        'reference' => 'required',
        'meta' => 'required'
    ];

    public function profile()
    {
        return $this->belongsTo(UserProfiles::class, 'profile_id');
    }


}
