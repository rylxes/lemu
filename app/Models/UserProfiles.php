<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Glorand\Model\Settings\Traits\HasSettingsTable;

/**
 * Class UserProfiles
 * @package App\Models
 * @version January 27, 2020, 5:15 pm UTC
 *
 * @property string title
 * @property string first_name
 * @property string other_name
 * @property string last_name
 * @property string sex
 * @property string dob
 * @property string marital_status
 * @property string profession
 * @property string employer
 * @property string home_address
 * @property string office_address
 * @property string email
 * @property string phone
 */
class UserProfiles extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;
    use HasSettingsTable;

    public $table = 'user_profiles';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'reference',
        'first_name',
        'other_name',
        'last_name',
        'sex',
        'dob',
        'marital_status',
        'profession',
        'employer',
        'home_address',
        'office_address',
        'email',
        'next_of_kin',
        'next_of_kin_phone',
        'phone'
    ];



    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'first_name' => 'string',
        'other_name' => 'string',
        'last_name' => 'string',
        'sex' => 'string',
        'dob' => 'string',
        'marital_status' => 'string',
        'profession' => 'string',
        'employer' => 'string',
        'home_address' => 'string',
        'office_address' => 'string',
        'email' => 'string',
        'next_of_kin' => 'string',
        'next_of_kin_phone' => 'string',
        'phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'first_name' => 'required',
        'last_name' => 'required',
        'sex' => 'required',
        //'dob' => 'required|date|date_format:d-m-Y',
        'marital_status' => 'required',
        'profession' => 'required',
        'employer' => 'required',
        'home_address' => 'required',
        'office_address' => 'required',
        'email' => 'required|email',
        'next_of_kin' => 'required',
        'next_of_kin_phone' => 'required',
        //'phone' => 'required|unique:user_profiles,phone',
        'phone' => 'required',
        'passport' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
    ];

    protected $appends = ['full_name'];

    public function getFullNameAttribute(){
        return $this->attributes['first_name']." ".$this->attributes['last_name'];
    }
    public function getTable()
    {
        return $this->table;
    }

    public function payments()
    {
        return $this->hasOne(Payment::class, 'profile_id')->orderByDesc('created_at');
    }

    function url_exists($url)
    {
        try {
            $headers = @get_headers($url);
            if ($headers && strpos($headers[0], '200')) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $exception) {
            return false;
        }
    }

    function getLogo($logo, $default = null)
    {
        if ($default == null) {
            $default = asset('img/default_product.png');
        }


        try {
            $res = $this->url_exists($logo);
        } catch (\Exception $exception) {
            $res = false;
        }
        if (!$res) {
            $logo = $default;
        }
        if (empty($logo)) {
            $logo = $default;
        }
        return $logo;
    }


    public function getUploadPhotoAttribute()
    {
        // dd(getLogo($this->attributes['upload_1_url']));
        return $this->getLogo($this->attributes['photo']);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10);

        $this
            ->addMediaConversion('my-conversion')
            ->greyscale()
            ->withResponsiveImages();
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('passport')
            ->useFallbackUrl('/img/default_product.png')
            ->useFallbackPath(public_path('/img/default_product.png'));
    }

    public function images()
    {
        return $this->hasOne('Spatie\MediaLibrary\Models\Media', 'model_id')
            ->where('model_type', 'App\Models\UserProfiles');
    }

    public function imagesWithCollection($collection = 'default')
    {
        return $this->hasOne('Spatie\MediaLibrary\Models\Media', 'model_id')
            ->where('model_type', 'App\Models\UserProfiles')
            ->where('collection_name', $collection);
    }

    public function getImage($id)
    {
        return $this->hasOne('Spatie\MediaLibrary\Models\Media', 'model_id')
            ->where('model_type', 'App\Models\UserProfiles')
            ->where('model_id', $id);
    }


}
