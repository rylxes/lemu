<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ModelSettings
 * @package App\Models
 * @version February 8, 2020, 4:06 pm UTC
 *
 * @property integer model_id
 * @property string model_type
 * @property string settings
 */
class ModelSettings extends Model
{
    //  use SoftDeletes;

    public $table = 'model_settings';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'model_id',
        'model_type',
        'settings'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'model_id' => 'integer',
        'model_type' => 'string',
        'settings' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'model_id' => 'required',
        'model_type' => 'required',
        'settings' => 'required'
    ];

    
}
