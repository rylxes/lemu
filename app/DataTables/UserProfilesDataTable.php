<?php

namespace App\DataTables;

use App\Models\UserProfiles;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;


class UserProfilesDataTable extends DataTable
{

    public $exportColumns = [
        ['title' => 'Title', 'data' => 'title', 'footer' => 'title'],
        ['title' => 'First Name', 'data' => 'first_name', 'footer' => 'first_name'],
        ['title' => 'Other Name', 'data' => 'other_name', 'footer' => 'other_name'],
        ['title' => 'Last Name', 'data' => 'last_name', 'footer' => 'last_name'],
        ['title' => 'Dob', 'data' => 'dob', 'footer' => 'dob'],
        ['title' => 'Gender', 'data' => 'sex', 'footer' => 'sex'],
        ['title' => 'Marital Status', 'data' => 'marital_status', 'footer' => 'marital_status'],
        ['title' => 'Employer', 'data' => 'employer', 'footer' => 'employer'],
        ['title' => 'Profession', 'data' => 'profession', 'footer' => 'profession'],
        ['title' => 'Email', 'data' => 'email', 'footer' => 'email'],
        ['title' => 'Phone', 'data' => 'phone', 'footer' => 'phone'],
        ['title' => 'Registration Number', 'data' => 'reference', 'footer' => 'reference'],
        ['title' => 'Home Address', 'data' => 'home_address', 'footer' => 'home_address'],
        ['title' => 'Office Address', 'data' => 'office_address', 'footer' => 'office_address'],
        ['title' => 'Next Of Kin', 'data' => 'next_of_kin', 'footer' => 'next_of_kin'],
        ['title' => 'Next Of Kin Phone', 'data' => 'next_of_kin_phone', 'footer' => 'next_of_kin_phone'],
        ['title' => 'Phone', 'data' => 'phone', 'footer' => 'phone']
    ];
    public $printColumns = [
        ['title' => 'Title', 'data' => 'title', 'footer' => 'title'],
        ['title' => 'First Name', 'data' => 'first_name', 'footer' => 'first_name'],
        ['title' => 'Other Name', 'data' => 'other_name', 'footer' => 'other_name'],
        ['title' => 'Last Name', 'data' => 'last_name', 'footer' => 'last_name'],
        ['title' => 'Dob', 'data' => 'dob', 'footer' => 'dob'],
        ['title' => 'Gender', 'data' => 'sex', 'footer' => 'sex'],
        ['title' => 'Marital Status', 'data' => 'marital_status', 'footer' => 'marital_status'],
        ['title' => 'Employer', 'data' => 'employer', 'footer' => 'employer'],
        ['title' => 'Profession', 'data' => 'profession', 'footer' => 'profession'],
        ['title' => 'Email', 'data' => 'email', 'footer' => 'email'],
        ['title' => 'Phone', 'data' => 'phone', 'footer' => 'phone'],
        ['title' => 'Registration Number', 'data' => 'reference', 'footer' => 'reference'],
        ['title' => 'Home Address', 'data' => 'home_address', 'footer' => 'home_address'],
        ['title' => 'Office Address', 'data' => 'office_address', 'footer' => 'office_address'],
        ['title' => 'Next Of Kin', 'data' => 'next_of_kin', 'footer' => 'next_of_kin'],
        ['title' => 'Next Of Kin Phone', 'data' => 'next_of_kin_phone', 'footer' => 'next_of_kin_phone'],
        ['title' => 'Phone', 'data' => 'phone', 'footer' => 'phone']
    ];


    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'user_profiles.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\UserProfiles $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UserProfiles $model)
    {
        return $model->whereHas('payments')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->removeColumn('reference', 'dob', 'other_name', 'employer', 'home_address', 'office_address', 'next_of_kin', 'next_of_kin_phone', 'phone')
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom' => 'Bfrtip',
                'stateSave' => true,
                'order' => [[0, 'desc']],
                'buttons' => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {

        return [
            ['title' => 'Title', 'data' => 'title', 'footer' => 'title'],
            ['title' => 'First Name', 'data' => 'first_name', 'footer' => 'first_name'],
            ['title' => 'Other Name', 'data' => 'other_name', 'footer' => 'other_name'],
            ['title' => 'Last Name', 'data' => 'last_name', 'footer' => 'last_name'],
            ['title' => 'Dob', 'data' => 'dob', 'footer' => 'dob'],
            ['title' => 'Gender', 'data' => 'sex', 'footer' => 'sex'],
            ['title' => 'Marital Status', 'data' => 'marital_status', 'footer' => 'marital_status'],
            ['title' => 'Employer', 'data' => 'employer', 'footer' => 'employer'],
            ['title' => 'Profession', 'data' => 'profession', 'footer' => 'profession'],
            ['title' => 'Email', 'data' => 'email', 'footer' => 'email'],
            ['title' => 'Phone', 'data' => 'phone', 'footer' => 'phone'],
            ['title' => 'Registration Number', 'data' => 'reference', 'footer' => 'reference'],
            ['title' => 'Home Address', 'data' => 'home_address', 'footer' => 'home_address'],
            ['title' => 'Office Address', 'data' => 'office_address', 'footer' => 'office_address'],
            ['title' => 'Next Of Kin', 'data' => 'next_of_kin', 'footer' => 'next_of_kin'],
            ['title' => 'Next Of Kin Phone', 'data' => 'next_of_kin_phone', 'footer' => 'next_of_kin_phone'],
            ['title' => 'Phone', 'data' => 'phone', 'footer' => 'phone']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'user_profilesdatatable_' . time();
    }
}
