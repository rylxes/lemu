<?php

namespace App\Http\Controllers;

use App\DataTables\PaymentDataTable;
use App\Events\AfterRegister;
use App\Http\Requests;
use App\Http\Requests\CreatePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Models\Payment;
use App\Models\UserProfiles;
use App\Repositories\PaymentRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Config;
use Response;
use Paystack;

class PaymentController extends AppBaseController
{
    /** @var  PaymentRepository */
    private $paymentRepository;

    public function __construct(PaymentRepository $paymentRepo)
    {
        $this->paymentRepository = $paymentRepo;
        $this->middleware('auth')->except(['redirectToGateway', 'handleGatewayCallback', 'handleGatewayWebhook']);
    }

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {

        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    public function handleGatewayWebhook()
    {
        $req = request()->ip();
        if (!in_array($req, ["52.31.139.75", "52.49.173.169", "52.214.14.220"]))
            exit();

        if ((strtoupper($_SERVER['REQUEST_METHOD']) != 'POST') || !array_key_exists('x-paystack-signature', $_SERVER))
            exit();

        // Retrieve the request's body
        $input = @file_get_contents("php://input");

// validate event do all at once to avoid timing attack
        if ($_SERVER['HTTP_X_PAYSTACK_SIGNATURE'] !== hash_hmac('sha512', $input, Config::get('paystack.secretKey')))
            exit();

        http_response_code(200);

// parse event (which is json string) as object
// Do something - that will not take long - with $event
        $event = json_decode($input);
        $each = @$event['data'];
        if ($event['event'] == 'charge.success') {
            try {
                $up = UserProfiles::where('reference', @$each['reference'])->first();
                if (!empty($up)) {
                    $input = [
                        'profile_id' => $up->id,
                        'amount' => $each['amount'] / 100,
                        'reference' => $each['reference'],
                        'meta' => json_encode($each),
                    ];
                    $this->paymentRepository->create($input);
                    event(new AfterRegister($up));
                }
            } catch (\Exception $exception) {

            }
        } else {
            $email = $each['customer']['email'];
            UserProfiles::where('email', $email)->delete();
        }

        exit();

    }

    /**
     * Obtain Paystack payment information
     *
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

        $status = @$paymentDetails['data']['status'];
        if ($status == 'success') {
            $payt = Payment::where('reference', $paymentDetails['data']['reference'])->first();
            if (empty($payt)) {
                try {
                    $up = UserProfiles::where('reference', $paymentDetails['data']['reference'])->first();
                    if (!empty($up)) {
                        $input = [
                            'profile_id' => $up->id,
                            'amount' => $paymentDetails['data']['amount'] / 100,
                            'reference' => $paymentDetails['data']['reference'],
                            'meta' => json_encode($paymentDetails),
                        ];
                        $this->paymentRepository->create($input);
                        event(new AfterRegister($up));
                    }

                } catch (\Exception $exception) {

                }
            }
        } else {
            $email = $paymentDetails['data']['customer']['email'];
            UserProfiles::where('email', $email)->delete();
        }
        Flash::success('You have been successfully Registered');
        return redirect(url('/'));
        // dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }


    /**
     * Display a listing of the Payment.
     *
     * @param PaymentDataTable $paymentDataTable
     * @return Response
     */
    public
    function index(PaymentDataTable $paymentDataTable)
    {
        return $paymentDataTable->render('payments.index');
    }

    /**
     * Show the form for creating a new Payment.
     *
     * @return Response
     */
    public
    function create()
    {
        return view('payments.create');
    }

    /**
     * Store a newly created Payment in storage.
     *
     * @param CreatePaymentRequest $request
     *
     * @return Response
     */
    public
    function store(CreatePaymentRequest $request)
    {
        $input = $request->all();

        $payment = $this->paymentRepository->create($input);

        Flash::success('Payment saved successfully.');

        return redirect(route('payments.index'));
    }

    /**
     * Display the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public
    function show($id)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        return view('payments.show')->with('payment', $payment);
    }

    /**
     * Show the form for editing the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public
    function edit($id)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        return view('payments.edit')->with('payment', $payment);
    }

    /**
     * Update the specified Payment in storage.
     *
     * @param  int $id
     * @param UpdatePaymentRequest $request
     *
     * @return Response
     */
    public
    function update($id, UpdatePaymentRequest $request)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        $payment = $this->paymentRepository->update($request->all(), $id);

        Flash::success('Payment updated successfully.');

        return redirect(route('payments.index'));
    }

    /**
     * Remove the specified Payment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public
    function destroy($id)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        $this->paymentRepository->delete($id);

        Flash::success('Payment deleted successfully.');

        return redirect(route('payments.index'));
    }
}
