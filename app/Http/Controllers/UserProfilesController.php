<?php

namespace App\Http\Controllers;

use App\DataTables\UnpaidUserProfilesDataTable;
use App\DataTables\UserProfilesDataTable;
use App\Events\AfterRegister;
use App\Http\Requests;
use App\Http\Requests\CreateUserProfilesRequest;
use App\Http\Requests\UpdateUserProfilesRequest;
use App\Models\UserProfiles;
use App\Repositories\UserProfilesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Composer;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Response;
use Paystack;
use Zipper;
use App\Paystack\Paystack as PayStackHack;
use Spatie\MediaLibrary\Models\Media;

class UserProfilesController extends AppBaseController
{
    /** @var  UserProfilesRepository */
    private $userProfilesRepository;
    protected $composer;

    public function __construct(UserProfilesRepository $userProfilesRepo, Composer $composer)
    {
        $this->userProfilesRepository = $userProfilesRepo;
        $this->composer = $composer;
        $this->middleware('auth')->except(['register', 'store', 'terms', 'getCount', 'signup', 'thanks']);
    }

    /**
     * Display a listing of the UserProfiles.
     *
     * @param UserProfilesDataTable $userProfilesDataTable
     * @return Response
     */
    public function index(UserProfilesDataTable $userProfilesDataTable)
    {
        $male = UserProfiles::whereHas('payments')->where('sex', 'Male')->count();
        $female = UserProfiles::whereHas('payments')->where('sex', 'Female')->count();
        return $userProfilesDataTable->render('user_profiles.index', compact('male', 'female'));
    }

    /**
     * Display a listing of the UserProfiles.
     *
     * @param UserProfilesDataTable $userProfilesDataTable
     * @return Response
     */
    public function unpaid(UnpaidUserProfilesDataTable $userProfilesDataTable)
    {
        $male = UserProfiles::whereDoesntHave('payments')->where('sex', 'Male')->count();
        $female = UserProfiles::whereDoesntHave('payments')->where('sex', 'Female')->count();
        return $userProfilesDataTable->render('user_profiles.index', compact('male', 'female'));
    }


    public function getCount()
    {
        //Artisan::call('cache:clear');
        // dd(request()->all());
        //return response()->json(['type' => false]);
        $gender = request()->all()['gender'];

        if ($gender == 'Male') {
            $male = getValue('Male');
            $count = UserProfiles::whereHas('payments')->where('sex', 'Male')->count();
            if ($count >= $male) {
                return response()->json(['type' => false]);
            }
            return response()->json(['type' => true]);
        } else if ($gender == 'Female') {
            $female = getValue('Female');
            $count = UserProfiles::whereHas('payments')->where('sex', 'Female')->count();
            if ($count >= $female) {
                return response()->json(['type' => false]);
            }
            return response()->json(['type' => true]);
        }
        return response()->json(['type' => false]);
    }

    /**
     * Show the form for creating a new UserProfiles.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_profiles.create');
    }


    public function terms()
    {
        return view('user_profiles.terms');
    }

    public function thanks()
    {
        return view('user_profiles.thanks');
    }

    public function signup()
    {

        $titles = config('constants.titles');
        $marital = config('constants.marital');
        return view('user_profiles.reg')->with(compact('titles', 'marital'));
    }


    function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function download_images()
    {


        try {
            //$this->composer->dumpAutoloads();
            $profiles = UserProfiles::whereHas('payments')->get();

            $appURL = config('app.url') . "/";
            $zipper = new \App\Zipper\Zipper;
            $zipper->make(storage_path('app/paidUsers.zip'));
            foreach ($profiles as $eachProfiles) {
                try {
                    if (!empty($eachProfiles->passport)) {
                        $filez = str_replace('http://localhost/', "", $eachProfiles->passport);
                        $file2 = str_replace($appURL, "", $filez);
                        $exists = File::exists($file2);
                        if ($exists) {
                            $name = $this->clean(@$eachProfiles->full_name);
                            $fullFiles = public_path($file2);
                            $zipper->add($fullFiles, $name);
                        }
                    }
                } catch (\Exception $exception) {
                    dd($exception->getMessage() . " " . $exception->getLine());
                }
            }
            if (empty($zipper->listFiles())) {
                flash()->error("No Files to download");
                return redirect()->back();
            }
            $zipper->close();
        } catch (\Exception $exception) {
            dd($exception->getMessage() . " " . $exception->getLine());
        }

        return response()->download(storage_path('app/paidUsers.zip'))->deleteFileAfterSend();
    }


    public function register()
    {
        //$up = UserProfiles::find(1);
        // event(new AfterRegister($up));
        //flash()->error("Maximum Number of Females Allowed");


        $titles = config('constants.titles');
        $marital = config('constants.marital');
        return view('user_profiles.create')->with(compact('titles', 'marital'));
    }


    /**
     * Store a newly created UserProfiles in storage.
     *
     * @param CreateUserProfilesRequest $request
     *
     * @return Response
     */
    public function store()
    {

        //dd($input);
        //CreateUserProfilesRequest $request
        ini_set('post_max_size', '120M');
        ini_set('upload_max_filesize', '120M');
        ini_set('memory_limit', '8G');

        $input = request()->all();
        //dd($request->all());
        //$input = $request->all();
        //dd($input = request()->all());
        $input['dob'] = $input['birth_date'] . "-" . $input['birth_month'] . "-" . $input['birth_year'];
        if ($input['sex'] == 'Male') {
            $male = getValue('Male');
            $count = UserProfiles::whereHas('payments')->where('sex', 'Male')->count();
            if ($count >= $male) {
                flash()->error("Maximum Number of Males Allowed");
                return redirect()->back();
            }
        } else if ($input['sex'] == 'Female') {
            $female = getValue('Female');
            $count = UserProfiles::whereHas('payments')->where('sex', 'Female')->count();
            if ($count >= $female) {
                flash()->error("Maximum Number of Females Allowed");
                return redirect()->back();
            }
        }


        $userProfiles = $this->userProfilesRepository->create($input);
        if (!empty(request()->file('passport'))) {
            try {
                $userProfiles->addMedia(request()->file('passport'))
                    ->usingName('Passport')
                    ->toMediaCollection('passport');
                $mediaItems = $userProfiles->getMedia('passport');
                $data['passport'] = $mediaItems[0]->getFullUrl();
                UserProfiles::whereId($userProfiles->id)->update($data);
            } catch (\Exception $exception) {
                flash()->error($exception->getMessage());
                return redirect()->back();
            }
        }


        $id = sprintf("%'03d", $userProfiles->id);
        $paymentRef = "LEMUITIF-2020-" . $id;
        if (config('app.env') == 'local') {
            $paymentRef = Paystack::genTranxRef();
        }

        request()->files->remove('passport');
        //dd($request->files);
        $payment = [
            "amount" => getValue('Amount') * 100,
            // "reference" => Paystack::genTranxRef(),
            "reference" => $paymentRef,
            "email" => $input['email'],
            "first_name" => $input['first_name'],
            "callback_url" => url('payment/callback'),
            "last_name" => $input['last_name'],
        ];

        $data['reference'] = $paymentRef;
        UserProfiles::whereId($userProfiles->id)->update($data);


        request()->merge($payment);
        $ps = new PayStackHack();
        return $ps->getAuthorizationUrl()->redirectNow();

//        Flash::success('You have been successfully Registered');
//        return redirect()->back();
    }

    /**
     * Display the specified UserProfiles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $logo = '';
        $media = Media::
        where('model_type', 'App\Models\UserProfiles')
            ->where('model_id', $id)
            ->where('name', 'Passport')
            ->first();

        if (!empty($media)) {
            //$logo = $media->getFullUrl('my-conversion');
            $logo = $media->getFullUrl();
        }

        //dd($logo);


        $userProfiles = UserProfiles::with('payments')->where('id', $id)->first();

        // $userProfiles = $this->userProfilesRepository->find($id)->with('payments')->get();


        // dd($userProfiles);
        if (empty($userProfiles)) {
            Flash::error('User Profiles not found');

            return redirect(route('userProfiles.index'));
        }

        return view('user_profiles.show')->with('userProfiles', $userProfiles)->with(compact('logo'));
    }

    /**
     * Show the form for editing the specified UserProfiles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userProfiles = $this->userProfilesRepository->find($id);

        if (empty($userProfiles)) {
            Flash::error('User Profiles not found');

            return redirect(route('userProfiles.index'));
        }

        return view('user_profiles.edit')->with('userProfiles', $userProfiles);
    }

    /**
     * Update the specified UserProfiles in storage.
     *
     * @param  int $id
     * @param UpdateUserProfilesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserProfilesRequest $request)
    {
        $userProfiles = $this->userProfilesRepository->find($id);

        if (empty($userProfiles)) {
            Flash::error('User Profiles not found');

            return redirect(route('userProfiles.index'));
        }

        $userProfiles = $this->userProfilesRepository->update($request->all(), $id);

        Flash::success('User Profiles updated successfully.');

        return redirect(route('userProfiles.index'));
    }

    /**
     * Remove the specified UserProfiles from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userProfiles = $this->userProfilesRepository->find($id);

        if (empty($userProfiles)) {
            Flash::error('User Profiles not found');

            return redirect(route('userProfiles.index'));
        }

        $this->userProfilesRepository->delete($id);

        Flash::success('User Profiles deleted successfully.');

        return redirect(route('userProfiles.index'));
    }
}
