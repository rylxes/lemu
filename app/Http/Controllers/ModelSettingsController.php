<?php

namespace App\Http\Controllers;

use App\DataTables\ModelSettingsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateModelSettingsRequest;
use App\Http\Requests\UpdateModelSettingsRequest;
use App\Repositories\ModelSettingsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ModelSettingsController extends AppBaseController
{
    /** @var  ModelSettingsRepository */
    private $modelSettingsRepository;

    public function __construct(ModelSettingsRepository $modelSettingsRepo)
    {
        $this->modelSettingsRepository = $modelSettingsRepo;
    }

    /**
     * Display a listing of the ModelSettings.
     *
     * @param ModelSettingsDataTable $modelSettingsDataTable
     * @return Response
     */
    public function index(ModelSettingsDataTable $modelSettingsDataTable)
    {
        return $modelSettingsDataTable->render('model_settings.index');
    }

    /**
     * Show the form for creating a new ModelSettings.
     *
     * @return Response
     */
    public function create()
    {
        return view('model_settings.create');
    }

    /**
     * Store a newly created ModelSettings in storage.
     *
     * @param CreateModelSettingsRequest $request
     *
     * @return Response
     */
    public function store(CreateModelSettingsRequest $request)
    {
        $input = $request->all();

        $modelSettings = $this->modelSettingsRepository->create($input);

        Flash::success('Model Settings saved successfully.');

        return redirect(route('modelSettings.index'));
    }

    /**
     * Display the specified ModelSettings.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $modelSettings = $this->modelSettingsRepository->find($id);

        if (empty($modelSettings)) {
            Flash::error('Model Settings not found');

            return redirect(route('modelSettings.index'));
        }

        return view('model_settings.show')->with('modelSettings', $modelSettings);
    }

    /**
     * Show the form for editing the specified ModelSettings.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $modelSettings = $this->modelSettingsRepository->find($id);

        if (empty($modelSettings)) {
            Flash::error('Model Settings not found');

            return redirect(route('modelSettings.index'));
        }

        return view('model_settings.edit')->with('modelSettings', $modelSettings);
    }

    /**
     * Update the specified ModelSettings in storage.
     *
     * @param  int              $id
     * @param UpdateModelSettingsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateModelSettingsRequest $request)
    {
        $modelSettings = $this->modelSettingsRepository->find($id);

        if (empty($modelSettings)) {
            Flash::error('Model Settings not found');

            return redirect(route('modelSettings.index'));
        }

        $modelSettings = $this->modelSettingsRepository->update($request->all(), $id);

        Flash::success('Model Settings updated successfully.');

        return redirect(route('modelSettings.index'));
    }

    /**
     * Remove the specified ModelSettings from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $modelSettings = $this->modelSettingsRepository->find($id);

        if (empty($modelSettings)) {
            Flash::error('Model Settings not found');

            return redirect(route('modelSettings.index'));
        }

        $this->modelSettingsRepository->delete($id);

        Flash::success('Model Settings deleted successfully.');

        return redirect(route('modelSettings.index'));
    }
}
