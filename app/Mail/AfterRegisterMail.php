<?php

namespace App\Mail;

use App\Models\UserProfiles;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AfterRegisterMail extends Mailable
{
    use Queueable, SerializesModels;
    public $userProfiles;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserProfiles $userProfiles)
    {
        $this->userProfiles = $userProfiles;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome')
            ->with([
                'userProfiles' => $this->userProfiles,
            ])->
            subject('LEMU Itikaf 2020/1441 Welcome Note');
    }
}
