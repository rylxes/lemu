<?php

namespace App\Events;

use App\Models\UserProfiles;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AfterRegister
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userProfiles;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UserProfiles $userProfiles)
    {
        $this->userProfiles = $userProfiles;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
