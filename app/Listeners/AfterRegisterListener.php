<?php

namespace App\Listeners;

use App\Events\AfterRegister;
use App\Mail\AfterRegisterMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class AfterRegisterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AfterRegister  $event
     * @return void
     */
    public function handle(AfterRegister $event)
    {
        try {
            $userProfiles = $event->userProfiles;
            $user = $userProfiles->email;
            Mail::to($user)->send(new AfterRegisterMail($userProfiles));
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
