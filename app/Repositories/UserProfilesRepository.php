<?php

namespace App\Repositories;

use App\Models\UserProfiles;
use App\Repositories\BaseRepository;

/**
 * Class UserProfilesRepository
 * @package App\Repositories
 * @version January 27, 2020, 5:15 pm UTC
*/

class UserProfilesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title_id',
        'first_name',
        'other_name',
        'last_name',
        'sex',
        'dob',
        'marital_status',
        'profession',
        'employer',
        'home_address',
        'office_address',
        'email',
        'phone'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserProfiles::class;
    }
}
