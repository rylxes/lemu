<?php

namespace App\Repositories;

use App\Models\ModelSettings;
use App\Repositories\BaseRepository;

/**
 * Class ModelSettingsRepository
 * @package App\Repositories
 * @version February 8, 2020, 4:06 pm UTC
*/

class ModelSettingsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'model_id',
        'model_type',
        'settings'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ModelSettings::class;
    }
}
